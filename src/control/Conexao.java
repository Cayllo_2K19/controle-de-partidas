package control;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexao {
	private Connection con;
	public Connection getCom() {
		return con;
	}
	public void setCom(Connection con) {
		this.con = con;
	}
	public void fecharConexao(){
		try {
			this.getCom().close();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: " + e.getMessage());
		}
	}
	public Conexao() {
		try {
			String host = "localhost";
			String user = "root";
			String pwd = "rodrigo123";
			String bd = "volei?useTimezone=true&serverTimezone=UTC";
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.setCom(DriverManager.getConnection("jdbc:mysql://"+host+"/"+bd,user,pwd));
			
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: " + e.getMessage());
		}
	}
	
}
